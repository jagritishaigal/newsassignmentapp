//
//  Utility.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 22/10/21.
//

import Foundation
import UIKit

let WHITE_COLOR = UIColor.white
let BLACK_COLOR = UIColor.black
let GRAY_COLOR = UIColor.lightGray
let BG_COLOR = UIColor.clear
let DARK_RED_COLOR = UIColor(red: 255/255, green: 96/255, blue: 96/255, alpha: 1.0)
let THEME_RED_COLOR = UIColor(red: 255/255, green: 186/255, blue: 186/255, alpha: 1.0)

let BORDER_COLOR = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
let SHADOW_COLOR = UIColor(red: 255/255, green: 96/255, blue: 96/255, alpha: 0.5)

let BASE_URL = "https://content.dailyfx.com/api/v1/"

let API_NEWS_MAIN = "dashboard"

let ALERT_TEXT = "Alert"
let OK_TEXT = "OK"
let NETWORK_TEXT = "Network Error!. Please try again later."

extension UIViewController {
    func addAlertView(title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    func addGradientColor(atView: UIView) {
        
        let gradient = CAGradientLayer()
        gradient.frame = atView.bounds
        
        let color1 = UIColor(named: "DarkRedColor")!
        let color2 = UIColor(named: "ThemeColor")!
        gradient.colors = [color1.cgColor, color2.cgColor]
        
        self.layer.insertSublayer(gradient, at: 0)
    }
   
    func setShadow() {
        self.layer.shadowColor = SHADOW_COLOR.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width:0.0, height:10.0)
        self.layer.shadowRadius = 10
    }
    
    func fadeOut(withDuration duration: TimeInterval = 3.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}

extension UILabel {
    func setFontRegular(size:CGFloat) {
        self.font = UIFont(name: "SFUIText-Regular", size: size)
    }
    
    func setFontBold(size: CGFloat) {
        self.font = UIFont(name: "SFUIText-SemiBold", size: size)
    }
    
    func setFontItalic(size: CGFloat) {
        self.font = UIFont(name: "SFUIText-RegularItalic", size: size)
    }
}

extension Int {
    func getDateFormat(ts: Int) -> String {
        let ti = TimeInterval(ts/1000)
        let date = Date(timeIntervalSince1970: ti)

        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy hh:mm a"
        
        let dateFormated = formatter.string(from: date)
        
        print("ts : \(ts), ti : \(ti), date :  \(date)")
        
        return dateFormated
    }
}

protocol DictionaryConvertor {
    func toDictionary() -> [String : Any]
}

extension DictionaryConvertor {
    
    func toDictionary() -> [String : Any] {
        let reflect = Mirror(reflecting: self)
        let children = reflect.children
        let dictionary = toAnyHashable(elements: children)
        return dictionary
    }
    
    func toAnyHashable(elements: AnyCollection<Mirror.Child>) -> [String : Any] {
        var dictionary: [String : Any] = [:]
        for element in elements {
            if let key = element.label {
                
                if let collectionValidHashable = element.value as? [AnyHashable] {
                    dictionary[key] = collectionValidHashable
                }
                
                if let validHashable = element.value as? AnyHashable {
                    dictionary[key] = validHashable
                }
                
                if let convertor = element.value as? DictionaryConvertor {
                    dictionary[key] = convertor.toDictionary()
                }
                
                if let convertorList = element.value as? [DictionaryConvertor] {
                    dictionary[key] = convertorList.map({ e in
                        e.toDictionary()
                    })
                }
            }
        }
        return dictionary
    }
}
