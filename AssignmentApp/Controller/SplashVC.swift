//
//  SplashVC.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 05/11/21.
//

import UIKit

class SplashVC: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var introLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpUI()
        setIntroText()
        animateText()
        
        let delayTime = DispatchTime.now() + 3.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.navigateNewsMainVC()
        })
    }
    
    func setUpUI() {
        view.backgroundColor = UIColor.lightGray
        backView.backgroundColor = THEME_RED_COLOR
        introLbl.backgroundColor = UIColor.clear
        introLbl.textColor = UIColor.white
        
        backView.addGradientColor(atView: backView)
        
        introLbl.setFontBold(size: 30.0)
    }
    
    func setIntroText() {
        introLbl.text = "Welcome to the News App!!"
    }
    
    func animateText() {
        introLbl.fadeOut()
    }
    
    func navigateNewsMainVC() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsMainVC") as! NewsMainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
