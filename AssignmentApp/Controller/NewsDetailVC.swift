//
//  NewsDetailVC.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 05/11/21.
//

import UIKit
import SDWebImage

class NewsDetailVC: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var bgScrollView: UIScrollView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var lastUpdatedLbl: UILabel!
    @IBOutlet weak var postedLbl: UILabel!
    
    var dataDict = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
        
        let rowsDict = dataDict
        let title = rowsDict["title"] as! String
        let displayTimestamp = rowsDict["displayTimestamp"] as? Int ?? 0
        let lastUpdatedTimestamp = rowsDict["lastUpdatedTimestamp"] as? Int ?? 0
        let authors = rowsDict["authors"] as? [[String: Any]] ?? []
        let headlineImageUrl = rowsDict["headlineImageUrl"] as? String ?? ""
        let description = rowsDict["description"] as? String ?? ""
        
        var authorName = ""
        var authorTitle = ""
        var authorIcon = ""
        if authors.count > 0 {
            authorName = authors[0]["name"] as? String ?? "NA"
            authorTitle = authors[0]["title"] as? String ?? "NA"
            authorIcon = authors[0]["photo"] as? String ?? "NA"
        }
        
        let lastUpdated = "Last updated: \(lastUpdatedTimestamp.getDateFormat(ts: lastUpdatedTimestamp))"
        let postedAt = "\(displayTimestamp.getDateFormat(ts: displayTimestamp))"
                
        titleLbl.text = title
        lastUpdatedLbl.text = lastUpdated
        postedLbl.text = postedAt
        authorLbl.text = "\(authorName), \n\(authorTitle)"
        descLbl.text = description
        
        headerImageView.image = UIImage(named: "unavailable")
        authorImageView.image = UIImage(named: "unavailable")
        
        if headlineImageUrl.count > 0 {
            headerImageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            headerImageView?.sd_setImage(with: URL(string: headlineImageUrl),
                                      placeholderImage: UIImage(named: "unavailable"),
                                      options: [],
                                      completed: { (image: UIImage?, error: Error?, cacheType: SDImageCacheType!, imageURL: URL?) in
                                        
                                        //guard let image = image else { return }
                                      })
        }
        
        if authorIcon.count > 0 {
            authorImageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            authorImageView?.sd_setImage(with: URL(string: authorIcon),
                                      placeholderImage: UIImage(named: "unavailable"),
                                      options: [],
                                      completed: { (image: UIImage?, error: Error?, cacheType: SDImageCacheType!, imageURL: URL?) in
                                        
                                        //guard let image = image else { return }
                                      })
        }
        
        print("rowsDict: \(title), \(lastUpdated), postedAt: \(postedAt)")
    }
    
    func setUpUI() {
        view.backgroundColor = THEME_RED_COLOR
        backView.backgroundColor = THEME_RED_COLOR
        
        topView.backgroundColor = THEME_RED_COLOR
        headerLbl.backgroundColor = UIColor.clear
        headerLbl.textColor = DARK_RED_COLOR
                
        bgScrollView.backgroundColor = UIColor.clear
        titleLbl.backgroundColor = UIColor.clear
        titleLbl.textColor = DARK_RED_COLOR
        descLbl.backgroundColor = UIColor.clear
        descLbl.textColor = .black
        authorLbl.backgroundColor = UIColor.clear
        authorLbl.textColor = DARK_RED_COLOR
        postedLbl.backgroundColor = UIColor.clear
        postedLbl.textColor = UIColor.darkGray
        lastUpdatedLbl.backgroundColor = UIColor.clear
        lastUpdatedLbl.textColor = UIColor.darkGray
                
        headerLbl.setFontBold(size: 24.0)
        titleLbl.setFontBold(size: 18.0)
        descLbl.setFontRegular(size: 15.0)
        authorLbl.setFontItalic(size: 13.0)
        postedLbl.setFontItalic(size: 12.0)
        lastUpdatedLbl.setFontItalic(size: 12.0)
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReadMoreClick(_ sender: UIButton) {
        let rowsDict = dataDict
        let urlStr = rowsDict["url"] as? String ?? ""
        
        if urlStr.count > 0 {
            if let url = URL(string: urlStr) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func btnAuthorClick(_ sender: UIButton) {
        let rowsDict = dataDict
        let authors = rowsDict["authors"] as? [[String: Any]] ?? []
              
        if authors.count > 0 {
            if let urlStr = authors[0]["bio"] as? String {
                if urlStr.count > 0 {
                    if let url = URL(string: urlStr) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
