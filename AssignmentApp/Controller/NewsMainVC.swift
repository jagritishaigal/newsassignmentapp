//
//  NewsMainVC.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 22/10/21.
//

import UIKit
import SVProgressHUD
import SDWebImage

class NewsMainVC: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var breakingNewsView: UIView!
    @IBOutlet weak var breakingNewsLbl: UILabel!
    @IBOutlet weak var categoriesCollView: UICollectionView!
    @IBOutlet weak var newsTblView: UITableView!
    
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    @IBOutlet weak var breakingNewsHeight: NSLayoutConstraint!
    
    var categoriesList: [Categories] = []
    var newsList = NewsMainModal()
    var dataArray = [[String: Any]]()
    var selectedTab = "topNews"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpUI()
        setCategories()
        getNewsList()
    }
    
    func setUpUI() {
        view.backgroundColor = THEME_RED_COLOR
        backView.backgroundColor = THEME_RED_COLOR
        
        topView.backgroundColor = THEME_RED_COLOR
        headerLbl.backgroundColor = UIColor.clear
        headerLbl.textColor = DARK_RED_COLOR
        
        breakingNewsView.backgroundColor = DARK_RED_COLOR
        breakingNewsLbl.backgroundColor = UIColor.clear
        breakingNewsLbl.textColor = WHITE_COLOR
        
        categoriesCollView.backgroundColor = UIColor.clear
        newsTblView.backgroundColor = UIColor.clear
        
        headerLbl.setFontBold(size: 24.0)
        breakingNewsLbl.setFontItalic(size: 15.0)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        categoriesCollView?.collectionViewLayout = layout
        categoriesCollView?.allowsMultipleSelection = true
        
        newsTblView.estimatedRowHeight = 70
        newsTblView.rowHeight = UITableView.automaticDimension
        newsTblView.tableFooterView = UIView()
    }
    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        
    }
    
    func navigateNewsMainVC() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setCategories() {
        // let c1 = Categories(key: "breakingNews", title: "Breaking News")
        let c2 = Categories(key: "topNews", title: "Top News")
        let c3 = Categories(key: "dailyBriefings", title: "Daily Briefings")
        let c4 = Categories(key: "technicalAnalysis", title: "Technical Analysis")
        let c5 = Categories(key: "specialReport", title: "Special Report")
        
        // categoriesList.append(c1)
        categoriesList.append(c2)
        categoriesList.append(c3)
        categoriesList.append(c4)
        categoriesList.append(c5)
    }
    
    func getNewsList() {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
        
        APIClient.sharedInstance.mewsMainAPI() {
            (response, success) in
            
            if (success && response != nil) {
                self.newsList = self.parseJsonData(data: response!)
                
                DispatchQueue.main.async() {
                    self.selectedTab = "topNews"
                    self.updateUI()
                    
                    SVProgressHUD.dismiss()
                }
            }
            else {
                DispatchQueue.main.async() {
                    self.addAlertView(title: "", message: NETWORK_TEXT, buttonTitle: OK_TEXT)
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func parseJsonData(data: [String: Any]) -> NewsMainModal {
        let decoder = JSONDecoder()
        
        var newsData = NewsMainModal()
        let dataObj = (try? JSONSerialization.data(withJSONObject: data, options: []))!
        
        
        do {
            let loanDataStore = try decoder.decode(NewsMainModal.self, from: dataObj)
            newsData = loanDataStore
            print("newsData : \(newsData)")
            
        } catch {
            print(error)
        }
        
        return newsData
    }
    
    func updateUI() {
        if let breakingNewsStr = newsList.breakingNews {
            let length = breakingNewsStr.count
            if length > 0 {
                breakingNewsLbl.text = breakingNewsStr
                breakingNewsHeight.constant =  breakingNewsHeight.constant
            }
            else {
                breakingNewsLbl.text = "Breaking News not available..."
                breakingNewsHeight.constant = 70 // 0.5
            }
        }
        else {
            breakingNewsLbl.text = "Breaking News not available..."
            breakingNewsHeight.constant = 70 // 0.5
        }
        
        //Updating tabs
        dataArray.removeAll()
        
        if selectedTab == "topNews" {
            let dataDict = newsList.toDictionary()
            let dataArray1 = dataDict["topNews"]
            let rowDict = ["header": selectedTab, "rows": dataArray1]
            dataArray.append(rowDict as [String : Any])
        }
        else if selectedTab == "dailyBriefings" {
            let dataDict = newsList.dailyBriefings
            let dataObj = dataDict?.toDictionary()
            
            let dataArray1 = dataObj?["eu"] ?? []
            let dataArray2 = dataObj?["asia"] ?? []
            let dataArray3 = dataObj?["us"] ?? []
            
            let rowDict1 = ["header": "Europe", "rows": dataArray1]
            let rowDict2 = ["header": "Asia", "rows": dataArray2]
            let rowDict3 = ["header": "United States", "rows": dataArray3]
            
            dataArray.append(rowDict1 as [String : Any])
            dataArray.append(rowDict2 as [String : Any])
            dataArray.append(rowDict3 as [String : Any])
        }
        else if selectedTab == "technicalAnalysis" {
            let dataDict = newsList.toDictionary()
            let dataArray1 = dataDict["technicalAnalysis"]
            let rowDict = ["header": selectedTab, "rows": dataArray1]
            dataArray.append(rowDict as [String : Any])
        }
        else if selectedTab == "specialReport" {
            let dataDict = newsList.toDictionary()
            let dataArray1 = dataDict["specialReport"]
            let rowDict = ["header": selectedTab, "rows": dataArray1]
            dataArray.append(rowDict as [String : Any])
        }
        
        print("newsList : \(newsList) \n dataArray : \(dataArray)")
        
        self.categoriesCollView.reloadData()
        self.newsTblView.reloadData()
    }
    
    func getLocalDateFormat(ts: Int) -> String {
        let ti = TimeInterval(ts/1000)
        let date = Date(timeIntervalSince1970: ti)

        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy hh:mm a"
        
        let dateFormated = formatter.string(from: date)
        
        print("ts : \(ts), ti : \(ti), date :  \(date)")
        
        return dateFormated
    }
}

extension NewsMainVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedTab == "dailyBriefings" {
            return 3
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArray.count > 0 {
            let rows = dataArray[section]["rows"] as! [[String: Any]]
            
            return rows.count
        }
        else {
            return 0
        }
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 100.0
     }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedTab == "dailyBriefings" {
            return 20
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedTab == "dailyBriefings" {
            let titleStr = dataArray[section]["header"] as! String
            
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
            let lbl = UILabel(frame: CGRect(x: 15, y: 0, width: headerView.frame.width - 15, height: 20))
            lbl.textColor = DARK_RED_COLOR
            lbl.setFontBold(size: 15.0)
            lbl.text = titleStr
            headerView.addSubview(lbl)
            return headerView
        }
        else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newsTblView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as! MainCell
        
        let rows = dataArray[indexPath.section]["rows"] as! [[String: Any]]
        let rowsDict = rows[indexPath.row]
        let title = rowsDict["title"] as! String
        let displayTimestamp = rowsDict["displayTimestamp"] as? Int ?? 0
        let lastUpdatedTimestamp = rowsDict["lastUpdatedTimestamp"] as? Int ?? 0
        let authors = rowsDict["authors"] as? [[String: Any]] ?? []
        let headlineImageUrl = rowsDict["headlineImageUrl"] as? String ?? ""
        
        var authorName = ""
        if authors.count > 0 {
            authorName = authors[0]["name"] as? String ?? "NA"
        }
        
        let lastUpdated = "Last updated: \(lastUpdatedTimestamp.getDateFormat(ts: lastUpdatedTimestamp))"
        let postedAt = "\(displayTimestamp.getDateFormat(ts: displayTimestamp))"
        
        print("title: \(title), \(lastUpdated), postedAt: \(postedAt)")
        
        cell.titleLbl.text = title
        cell.lastUpdatedLbl.text = lastUpdated
        cell.postedLbl.text = postedAt
        cell.authorLbl.text = "Author: \(authorName)"
        
        if headlineImageUrl.count > 0 {
            cell.headerImageView?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.headerImageView?.sd_setImage(with: URL(string: headlineImageUrl),
                                              placeholderImage: UIImage(named: "unavailable"),
                                              options: [],
                                              completed: { (image: UIImage?, error: Error?, cacheType: SDImageCacheType!, imageURL: URL?) in
                                                
                                                //guard let image = image else { return }
                                              })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rows = dataArray[indexPath.section]["rows"] as! [[String: Any]]
        let rowsDict = rows[indexPath.row]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
        vc.dataDict = rowsDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


var storedOffsets = [Int: CGFloat]()

extension NewsMainVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCVCell", for: indexPath) as! CategoryCVCell
        
        let category = categoriesList[indexPath.row]
        let key = category.key
        let title = category.title
        
        cell.titleLbl?.text = title
        
        if key == selectedTab {
            cell.bgView?.backgroundColor = DARK_RED_COLOR
        }
        else {
            cell.bgView?.backgroundColor = BG_COLOR
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70.0, height: 30.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categoriesList[indexPath.row]
        let key = category.key
        
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCVCell
        cell.bgView?.backgroundColor = DARK_RED_COLOR
        
        selectedTab = key
        updateUI()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}
