//
//  AuthorModal.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 07/11/21.
//

import Foundation

struct AuthorModal: Codable, DictionaryConvertor {
    let title: String?
    let name: String?
    let bio: String?
    let photo: String?
    let descriptionShort: String?
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
}
