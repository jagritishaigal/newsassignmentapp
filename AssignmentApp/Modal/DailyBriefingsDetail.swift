//
//  DailyBriefingsDetail.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 05/11/21.
//

import Foundation

struct DailyBriefingsDetail: Codable, DictionaryConvertor {
    let title: String?
    let url: String?
    let description: String?
    let headlineImageUrl: String?
    let displayTimestamp: Int?
    let lastUpdatedTimestamp: Int?
    let authors: [AuthorModal]?
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
}
