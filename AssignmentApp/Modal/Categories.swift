//
//  Categories.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 05/11/21.
//

import Foundation

struct Categories: Codable {
    var key: String
    var title: String
}
