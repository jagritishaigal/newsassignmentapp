//
//  DailyBriefings.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 05/11/21.
//

import Foundation

struct DailyBriefings: Codable, DictionaryConvertor {
    let eu: [DailyBriefingsDetail]?
    let asia: [DailyBriefingsDetail]?
    let us: [DailyBriefingsDetail]?
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
}
