//
//  APIClient.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 22/10/21.
//

import Foundation
import Reachability

enum APIError: Error {
    case invalidURL
    case requestFailed
    case networkUnreachable
    case unknown
}

enum APIMethod: String {
    case POST
    case GET
    case PUT
    case DELETE
}

class APIClient {
    
    typealias APIClientCompletion = (HTTPURLResponse?, Data?, APIError?) -> Void
    typealias APIRequestCompletion = ([String: Any]?, Bool) -> Void
    
    static let sharedInstance = APIClient()
    private let session = URLSession.shared
    private let baseURL = URL(string: BASE_URL)
    
    func getRequest(method: APIMethod, path: String, _ completion: @escaping APIClientCompletion) {
        guard let url = baseURL?.appendingPathComponent(path) else {
            completion(nil, nil, .invalidURL); return
        }
        
        do {
            let reachability = try Reachability()
            reachability.whenUnreachable = { _ in
                completion(nil, nil, .networkUnreachable); return
            }
        }
        catch {
            completion(nil, nil, .networkUnreachable); return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, nil, .requestFailed); return
            }
            completion(httpResponse, data, nil)
        }
        
        task.resume()
    }

    func getAPI(urlString: String, completionHandler: @escaping APIRequestCompletion) {
        self.getRequest(method: .GET, path: urlString) { (_, data, errorCode) in
            if let data = data, let result = String(data: data, encoding: .utf8) {
                self.logAPIResponse(endpoint: urlString, res: result)
                
                do{
                    let json = try JSONSerialization.jsonObject(with:data,options:[])
                    completionHandler(json as? [String : Any], true)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, false)
                }
            }
            else {
                self.logAPIError(error: errorCode ?? APIError.unknown)
                completionHandler(nil, false)
            }
        }
    }
    
    func mewsMainAPI(completionHandler:@escaping APIRequestCompletion) {
        self.getAPI(urlString: API_NEWS_MAIN) { (jsonDict, errorCode) in
            if jsonDict != nil && errorCode == true {
                completionHandler(jsonDict, true)
            }
            else {
                completionHandler(nil, false)
            }
        }
    }
    
    func logAPIResponse(endpoint: String, res: String) {
        print("API Endpoint : \(endpoint), Response : \n" + res)
    }
    
    func logAPIError(error: APIError) {
        var errorString = ""
        switch error {
        case .invalidURL:
            errorString = "Invalid API url passed."
        case .requestFailed:
            errorString = "API Request Failed."
        case .networkUnreachable:
            errorString = "Network unreachable."
        default:
            errorString = "API Passed."
        }
        print("API Error : " + errorString + "\n" + "Description : " + error.localizedDescription)
    }
}
