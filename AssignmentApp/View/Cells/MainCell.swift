//
//  MainCell.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 22/10/21.
//

import UIKit

class MainCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var lastUpdatedLbl: UILabel!
    @IBOutlet weak var postedLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        bgView.backgroundColor = BG_COLOR
        bgView.layer.cornerRadius = 6.0
        bgView.layer.borderColor = BORDER_COLOR.cgColor
        bgView.layer.borderWidth = 0.5
        bgView.clipsToBounds = true
        
        bgView.setShadow()
        
        headerImageView.backgroundColor = DARK_RED_COLOR
        titleLbl.backgroundColor = UIColor.clear
        authorLbl.backgroundColor = UIColor.clear
        lastUpdatedLbl.backgroundColor = UIColor.clear
        postedLbl.backgroundColor = UIColor.clear
        
        titleLbl.textColor = DARK_RED_COLOR
        authorLbl.textColor = UIColor.darkGray
        lastUpdatedLbl.textColor = UIColor.darkGray
        postedLbl.textColor = UIColor.darkGray
        
        titleLbl.setFontBold(size: 15.0)
        authorLbl.setFontItalic(size: 12.0)
        lastUpdatedLbl.setFontItalic(size: 12.0)
        postedLbl.setFontRegular(size: 12.0)
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
