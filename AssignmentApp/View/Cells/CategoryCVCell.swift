//
//  CategoryCVCell.swift
//  AssignmentApp
//
//  Created by JagritiShaigal on 22/10/21.
//

import UIKit

class CategoryCVCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgView.backgroundColor = BG_COLOR
        bgView.layer.cornerRadius = 6.0
        bgView.layer.borderColor = BORDER_COLOR.cgColor
        bgView.layer.borderWidth = 0.5
        bgView.clipsToBounds = true
        
        bgView.setShadow()
        
        titleLbl.backgroundColor = UIColor.clear
        titleLbl.textColor = UIColor.white
        
        titleLbl.setFontBold(size: 15.0)
    }
}
